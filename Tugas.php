<?php
include 'lib/Connection.php';

class Tugas{
    
    public function showDatabase()
    {
        $c = new Connection;
        $exec = mysqli_query($c->connect(), "show databases");
        while($fetch = mysqli_fetch_array($exec)){
            echo "Databases : ".$fetch['Database']."<br>";
            // print_r($fetch);
        }
    }

    public function showTable($database = "")
    {
        if($database != null){
            $c = new Connection;
            if(!mysqli_query($c->connect($database), "show tables")){
                echo "<h3>gagal mengkoneksi kan database $database. database tidak ditemukan</h3>";
            }else{
                $exec = mysqli_query($c->connect($database), "show tables");
                while($fetch = mysqli_fetch_array($exec)){
                    echo "Table : ".$fetch['Tables_in_'.$database]."<br>";
                    // print_r($fetch);
                }
            }
        }else{
            echo "masukkan table anda";
        }
    }

    function mysqli_field_name($result, $field_offset) {
        $properties = mysqli_fetch_field_direct($result, $field_offset);
        return is_object($properties) ? $properties->name : null;
    }

    public function showData($database = "", $table = "")
    {
        if($database != null && $table != null){
            $c = new Connection;
            $exec = mysqli_query($c->connect($database), "select * from ".$table);
            if(!mysqli_query($c->connect($database), "select * from ".$table)){
                echo "table tidak ditemukan";
            }else{
                echo "<table border='1px'>";
                echo "<thead>";
                echo "<tr>";
                $j = 0;
                for($i = 0; $i < mysqli_num_fields($exec); $i++){
                    $t = new Tugas;
                    $col = $t->mysqli_field_name($exec, $j++)." ";
                    echo "<td>".$col."</td>";
                }
                echo "</tr>";
                while($fetch = mysqli_fetch_array($exec)){
                    echo "<tr>";
                    $m = 0;
                    echo "</tr><tr>";
                    for($i = 0; $i < mysqli_num_fields($exec); $i++){
                        $t = new Tugas;
                        // $col = $t->mysqli_field_name($exec, $j++)." ";

                        // echo $fetch[$j++]."<br>";
                        echo "<td>".$fetch[$m++]."</td>";
                    }
                    echo "</tr>";
                }
                
                echo "</thead>";
                echo "</table>";
            }
        }else{
            echo "masukkan table anda dan columnnya";
        }
    }
}

// $t = new Tugas;
// echo "======[SHOW DATABASE]=======<br>";
// $t->showDatabase();
// echo "<br>======[SHOW TABLE]=======<br>";
// $t->showTable("mansonry");
// echo "<br>======[SHOW DATA]=======<br>";
// $t->showData("mansonry", "admins");