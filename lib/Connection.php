<?php

class Connection{

    public function connect($database = "")
    {
        $connect = mysqli_connect("localhost", "root", "", $database);
        return $connect;
    }

    public function query($con, $query)
    {
        $exec = mysqli_query($con, $query);
        $fetch = mysqli_fetch_array($exec);
        return $exec;
    }
}